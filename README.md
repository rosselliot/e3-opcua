# e3-opcua  

ESS Site-specific EPICS module : opcua

## requirements

- `patchelf`

## e3-opcua secondary build

Due to licensing requirements, we have split the opcua build process into two steps. The
first step is run by the license holder ([Karl Vestin](mailto:karl.vestin@ess.eu), at the
moment). 

The second step can be run by anyone, and uses this repository. This installs the shared
library as a vendor library which is then loaded by the e3 module `opcua`.

For more information, please see the [build instructions](docs/build.md).

## Automatic testing

[![pipeline status](https://gitlab.esss.lu.se/e3/common/e3-opcua/badges/master/pipeline.svg)](https://gitlab.esss.lu.se/e3/common/e3-opcua/-/commits/master) 

GitLab CI is used for automatic regression testing of this module. For more information, see [test setup](test/README.md).

