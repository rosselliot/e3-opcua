# Local installation

For require 3.3.0, local installation does not permit installation of vendor libraries.
There is a workaround, but it is slightly convoluted. In the following, I assume that you
will have mounted the e3 share (see [some page about mounting the e3 share](https://confluence.esss.lu.se/display/e3)).

There are three main stages:

1. Preparing the wrapper and the module
2. Building the module
3. Patching the binaries

## Preparing the wrapper and module

As you are reading this README, you are probably at least partway there. Nevertheless, these
instructions will start a few steps back.

1. First, you need to clone the e3 wrapper and ensure you are on the correct branch.
   ```bash
   $ git clone https://gitlab.esss.lu.se/e3/common/e3-opcua.git
   $ cd e3-opcua
   $ git checkout $TEST_BRANCH_NAME
   ```
2. Next, you need to initialize the submodule.
   ```bash
   $ make init
   $ cd opcua
   $ git checkout master
   $ cd ..
   ```
3. Next, you need to update the configuration files. Open the file `configure/CONFIG_MODULE` in
   your favorite text editor.
   * Modify the line `EPICS_MODULE_TAG:=...` to be `# EPICS_MODULE_TAG:=...`
   * Change the e3 module version to be something appropriate, e.g.
     ```
     E3_MODULE_VERSION:=test-module
     ```
     For the remainder of this, we will assume that `E3_MODULE_VERSION` is `test-module`. If you
     prefer a different version, please make the appropriate changes throughout.
4. As described above, you should have two files: `libopcuabuild.so` and `opcuabuild.dbd`. Place
   these in the following paths:
   * `opcua/opcuaSupport/os/linux-x86_64/libopcuabuild.so`
   * `opcua/opcuaSupport/dbd/opcuabuild.dbd`

## Building the module

You are now ready to build the module

1. To build the module, run
   ```bash
   $ make build
   ```
2. We do not want to perform a global install of the module (which you cannot do as the e3 share
   has been mounted read-only), so we use the local install method. Run:
   ```bash
   $ make cellinstall
   ```
   This will install the module in the subdirectory `cellMods` within the e3-opcua directory.

## Patching the binaries

The opcua module expects to find its vendor libraries under the `$(E3_SITELIBS_DIR)`, which you do
not have permission to write to. We can put the vendor libraries in the cellMods path, but the binaries
are going to look in the wrong place for the vendor libs, so we need to fix that.

1. Set the library and other files to be writeable:
   ```bash
   $ chmod -R a+w cellMods
   ```
2. Copy the libraries to the cellMods directory:
   ```bash
   $ cp opcua/opcuaSupport/os/linux-x86_64/lib*.so cellMods/opcua/test-module/lib/linux-x86_64/
   ```
3. Next, we need to change the shared library search path using `patchelf`.
   ```bash
   $ patchelf --remove-rpath cellMods/opcua/test-module/lib/linux-x86_64/libopcua.so
   $ patchelf --set-rpath '$ORIGIN' cellMods/opcua/test-module/lib/linux-x86_64/libopcua.so
   ```
   You can confirm that this has worked by using `readelf`:
   ```bash
   $ readelf -d cellMods/opcua/test-module/lib/linux-x86_64/libopcua.so

   Dynamic section at offset 0x6000 contains 33 entries:
     Tag        Type                         Name/Value
    0x000000000000001d (RUNPATH)            Library runpath: [$ORIGIN]
    0x0000000000000001 (NEEDED)             Shared library: [libopcuabuild.so]
    0x0000000000000001 (NEEDED)             Shared library: [libpthread.so.0]
    0x0000000000000001 (NEEDED)             Shared library: [librt.so.1]
    0x0000000000000001 (NEEDED)             Shared library: [libdl.so.2]
    0x0000000000000001 (NEEDED)             Shared library: [libstdc++.so.6]
    0x0000000000000001 (NEEDED)             Shared library: [libm.so.6]
    0x0000000000000001 (NEEDED)             Shared library: [libgcc_s.so.1]
    0x0000000000000001 (NEEDED)             Shared library: [libc.so.6]
    0x000000000000000e (SONAME)             Library soname: [libopcua.so]
   ...
   ```
   should have `Library runpath: [$ORIGIN]`.

## Running an IOC with the local module

Having followed the above steps, you should have a version of opcua built in the directory
`cellMods` within the e3-opcua repository. In order to use this version, you should run
the following command:
```bash
$ iocsh.bash -l cellMods -r opcua,test-module
```
which will tell require to look within the directory cellMods for the version `test-module` of
opcua.

If this works correctly, then you should see output that looks something like the following log.
```bash
$ iocsh.bash -l cellMods -r opcua,test-version
Warning: environment file /home/simonrose/data/git/e3/modules/e3-opcua-test/env.sh does not exist.

registerChannelProviderLocal firstTime true
#
# Start at "2020-W46-Nov10-1319-54-CET"
#
# Version information:
# European Spallation Source ERIC : iocsh.bash (3.3.0-PID-16524)
#
# --->--> snip -->--> 
# Please Use Version and other environment variables
# in order to report or debug this shell
#
# HOSTDISPLAY=""
# WINDOWID=""
# PWD="/home/simonrose/data/git/e3/modules/e3-opcua-test"
# USER="simonrose"
# LOGNAME="simonrose"
# EPICS_HOST_ARCH="linux-x86_64"
# EPICS_BASE="/epics/base-7.0.4"
# E3_REQUIRE_NAME="require"
# E3_REQUIRE_VERSION="3.3.0"
# E3_REQUIRE_LOCATION="/epics/base-7.0.4/require/3.3.0"
# E3_REQUIRE_BIN="/epics/base-7.0.4/require/3.3.0/bin"
# E3_REQUIRE_DB="/epics/base-7.0.4/require/3.3.0/db"
# E3_REQUIRE_DBD="/epics/base-7.0.4/require/3.3.0/dbd"
# E3_REQUIRE_INC="/epics/base-7.0.4/require/3.3.0/include"
# E3_REQUIRE_LIB="/epics/base-7.0.4/require/3.3.0/lib"
# EPICS_DRIVER_PATH="/epics/base-7.0.4/require/3.3.0/siteMods:/epics/base-7.0.4/require/3.3.0/siteApps"
# EPICS_CA_AUTO_ADDR_LIST=""
# EPICS_CA_ADDR_LIST=""
# PATH="/epics/base-7.0.4/require/3.3.0/bin:/epics/base-7.0.4/bin/linux-x86_64:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/home/simonrose/.local/bin:/home/simonrose/bin"
# --->--> snip -->--> 
#
# Set REQUIRE_IOC for its internal PVs
epicsEnvSet REQUIRE_IOC "REQMOD:localhost-16524"
#
# Enable an exit subroutine for sotfioc
dbLoadRecords "/epics/base-7.0.4/db/softIocExit.db" "IOC=REQMOD:localhost-16524"
#
# Set E3_IOCSH_TOP for the absolute path where iocsh.bash is executed.
epicsEnvSet E3_IOCSH_TOP "/home/simonrose/data/git/e3/modules/e3-opcua"
#
# 
# Load require module, which has the version 3.3.0
# 
dlload /epics/base-7.0.4/require/3.3.0/lib/linux-x86_64/librequire.so
dbLoadDatabase /epics/base-7.0.4/require/3.3.0/dbd/require.dbd
require_registerRecordDeviceDriver
Loading module info records for require
# 
epicsEnvSet EPICS_DRIVER_PATH cellMods:/epics/base-7.0.4/require/3.3.0/siteMods:/epics/base-7.0.4/require/3.3.0/siteApps
require opcua,0f433d8
Module opcua version test-version found in cellMods/opcua/test-version/
Loading library cellMods/opcua/test-version/lib/linux-x86_64/libopcua.so
Loaded opcua version test-version
Loading dbd file cellMods/opcua/test-version/dbd/opcua.dbd
Calling function opcua_registerRecordDeviceDriver
Loading module info records for opcua
# Set the IOC Prompt String One 
epicsEnvSet IOCSH_PS1 "localhost-16524 > "
#
# 
iocInit
Starting iocInit
############################################################################
## EPICS R7.0.4-E3-7.0.4-patch
## Rev. 2020-07-02T14:38+0200
############################################################################
OPC UA Client Device Support 0.8.0-dev (@EPICS_OPCUA_GIT_COMMIT@); using Unified Automation C++ Client SDK v1.7.1-476
iocRun: All initialization complete
localhost-16524 > 
```
