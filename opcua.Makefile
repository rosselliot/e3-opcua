#
#  Copyright (c) 2018 - 2019  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# Author  : Jeong Han Lee
# email   : jeonghan.lee@gmail.com
# Date    : Monday, October  7 12:19:55 CEST 2019
# version : 0.4.4
#
# Second author: Simon Rose
# email        : simon.rose@ess.eu
# Date         : Thursday, October 1
# Version      : 0.5.3


## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
include $(E3_REQUIRE_CONFIG)/DECOUPLE_FLAGS
#include $(where_am_I)/configure/CONFIG_OPCUA_VERSION


## Exclude linux-ppc64e6500
EXCLUDE_ARCHS += linux-ppc64e6500
EXCLUDE_ARCHS += linux-corei7-poky

SUPPORT:=opcuaSupport
SUPPORTDBD:=$(SUPPORT)/dbd

APP:=opcuaApp
APPDB:=$(APP)/Db

EXAMPLETOP=../opcua-upstream/exampleTop
EXAMPLETOPDB=$(EXAMPLETOP)/TemplateDbSup/AnyServerDb

#USR_CXXFLAGS_Linux += -std=c++11
#USR_CXXFLAGS += -DUSE_TYPED_RSET


ifeq ($(T_A),linux-x86_64)
USR_LDFLAGS += -Wl,--enable-new-dtags
USR_LDFLAGS += -u opcua_registerRecordDeviceDriver

LIB_SYS_LIBS += opcuabuild
endif

# This library is build in a first stage by the developer who has a license to the UA SDK.
# The output of that should be a library, libopcuabuild.so, and a .dbd file, opcua.dbd which
# need to be added to the epics-modules/opcua repository.
#
# The CORELIB line is still necessary here, as this is what tells driver.Makefile where to look
# for certain undefined symbols. TODO: Find a better way to do this???
MODULE_LIBS += $(SUPPORT)/os/linux-x86_64/libopcuabuild.so
CORELIB_Linux += $(addprefix ../,$(MODULE_LIBS))
DBDS    += $(SUPPORTDBD)/opcuabuild.dbd

# These are the vendor libraries from the UA SDK. These also need to be added to the epics-modules/opcua 
# reopsitory.
VENDOR_LIBS += libuabasecpp.so
VENDOR_LIBS += libuaclientcpp.so
VENDOR_LIBS += libuapkicpp.so
VENDOR_LIBS += libuastack.so
VENDOR_LIBS += libxmlparsercpp.so

VENDOR_LIBS += $(notdir $(MODULE_LIBS))

VENDOR_LIBS := $(addprefix $(SUPPORT)/os/linux-x86_64/,$(VENDOR_LIBS))

SCRIPTS += $(wildcard ../iocsh/*.iocsh)

USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)
USR_DBFLAGS += -I $(EXAMPLETOPDB)
USR_DBFLAGS += -I ../template

SUBS=$(wildcard $(APPDB)/*-ess.substitutions)
#TMPS+=$(wildcard $(APPDB)/*.template)
#TMPS+=$(wildcard $(APPDB)/*.db)
TEMPLATES += $(wildcard $(APPDB)/*.template)
TEMPLATES += $(wildcard $(APPDB)/*-ess.db)

db: $(SUBS) $(TMPS)

$(SUBS):
	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db -S $@ > $(basename $(@)).db.d
	$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db -S $@

$(TMPS):
	@printf "Inflating database ... %44s >>> %40s \n" "$@" "$(basename $(@)).db"
	@rm -f  $(basename $(@)).db.d  $(basename $(@)).db
	@$(MSI) -D $(USR_DBFLAGS) -o $(basename $(@)).db $@  > $(basename $(@)).db.d
	@$(MSI)    $(USR_DBFLAGS) -o $(basename $(@)).db $@


.PHONY: db $(SUBS) $(TMPS)


prebuild: $(MODULE_LIBS)
	$(QUIET) patchelf --set-rpath "\$$ORIGIN" $^

